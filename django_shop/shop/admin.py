from django.contrib import admin
from shop.models import (
    ShippingAddress,
    BillingAddress,
    Category,
    Customer,
    List,
    ListItem,
    Product,
    CartListItem,
    Cart,
    Order,
    OrderPayment,
    OrderItem,
)
from django.utils.safestring import mark_safe
from admin_numeric_filter.admin import (
    NumericFilterModelAdmin,
    SingleNumericFilter,
    RangeNumericFilter,
    SliderNumericFilter,
)


class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "category",
        "link_display",
        "img_display",
        "price",
        "available",
    )

    list_filter = (("price", RangeNumericFilter), "category", "available")

    search_fields = ("url", "name", "category")

    @mark_safe
    def link_display(self, obj):
        return f"<a href='{obj.url}'>مشاهده در سایت مبدأ</a>\n"

    @mark_safe
    def img_display(self, obj):
        return f"<img height='48px' src='{obj.photo_url}'>\n"


class CateogoriesItemAdmin(admin.ModelAdmin):
    list_display = ("category_name", "item_name", "item_description")


class CartListItemInline(admin.TabularInline):
    model = CartListItem
    extra = 1


class CartAdmin(admin.ModelAdmin):
    inlines = (CartListItemInline,)
    # list_display = ("get_items",)
    fields = ("customer", "extra", "created_at", "updated_at", "get_items")
    readonly_fields = ("created_at", "updated_at", "get_items")


class ListItemAdmin(admin.ModelAdmin):
    inlines = (CartListItemInline,)


admin.site.register(ShippingAddress)
admin.site.register(BillingAddress)
admin.site.register(Category)
admin.site.register(Customer)
admin.site.register(List)
admin.site.register(ListItem, ListItemAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(CartListItem)
admin.site.register(Cart, CartAdmin)
admin.site.register(Order)
admin.site.register(OrderPayment)
admin.site.register(OrderItem)

