from django.urls import path, re_path, reverse
from users.views import profile
from shop import views
from shop.views import general
from shop.views import cart

urlpatterns = [
    path("", general.index, name="index"),
    # path("books/", views.BookListView.as_view(), name="books"),
    path(
        "product/<int:pk>/", general.ProductDetailView.as_view(), name="product-detail"
    ),
    path("cart/detail/", views.cart.cart_detail, name="cart-detail"),
    path("list/add/", general.ListCreateView.as_view(), name="list-create"),
    path("list/<int:pk>/", general.ListDetailView.as_view(), name="list-detail"),
    path("api/get-product/<int:pk>/", general.get_product, name="product-api"),
    path("ajax/add-item-to-list/", general.add_item_to_list, name="add-item-to-list"),
    path(
        "ajax/remove-item-from-cart/",
        cart.remove_item_from_cart,
        name="remove-item-from-cart",
    ),
    path("ajax/add-item-to-cart/", cart.add_item_to_cart, name="add-item-to-cart"),
    path("ajax/reload-cart-items/", cart.reload_cart_items, name="reload-cart-items"),
    path("ajax/update_cart_items/", cart.update_cart_items, name="update-cart-items"),
    path("proceed-to-buy/", cart.proceed_to_buy, name="proceed-to-buy"),
    path("categories/", general.CategoryListView.as_view(), name="category-list"),
    path(
        "categories/<slug:slug>/",
        general.CategoryDetailView.as_view(),
        name="category-detail",
    ),
    path("<slug:username>/", general.show_user_lists, name="user-lists"),
    # for something like 'the-secret-garden' as slug
    # re_path(r'^book/(?P<stub>[-\w]+)$', views.BookDetailView.as_view(), name='book-detail'),
    # urls can have additional options
    # path('url/', views.my_reused_view, {'my_template_name': 'some_path'}, name='aurl'),
    # path('anotherurl/', views.my_reused_view, {'my_template_name': 'another_path'}, name='anotherurl'),
]

