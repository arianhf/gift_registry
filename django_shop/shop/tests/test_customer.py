# import pytest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.backends.cache import SessionStore
from shop.models.customer import VisitingCustomer

from django.test import TestCase, Client
from django.test.client import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from shop.models import Customer


class CustomerTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()

    def test_visiting_customer(self):
        request = self.factory.get("/shop")
        request.user = AnonymousUser()
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        customer = Customer.objects.get_from_request(request)
        customer.save()
        assert isinstance(customer, VisitingCustomer)
        assert str(customer) == "Visitor"
        assert customer.is_anonymous is True
        assert customer.is_authenticated is False
        assert customer.is_recognized is False
        assert customer.is_guest is False
        assert customer.is_registered is False
        assert customer.is_visitor is True

    def test_unrecognized_customer(self):
        """
        Check that an anonymous user creates an unrecognized customer.
        """
        request = self.factory.get("/shop")
        request.user = AnonymousUser()
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        customer = Customer.objects.get_or_create_from_request(request)
        assert isinstance(customer, Customer)
        assert customer.is_anonymous is True
        assert customer.is_authenticated is False
        assert customer.is_recognized is False
        assert customer.is_guest is False
        assert customer.is_registered is False
        assert customer.is_visitor is False

    def test_unexpired_customer(self):
        """
        Check that an anonymous user creates an unrecognized customer using the current session-key.
        """
        request = self.factory.get("/shop")
        request.user = AnonymousUser()
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()
        customer = Customer.objects.get_or_create_from_request(request)

        assert isinstance(customer, Customer)
        assert customer.is_anonymous is True
        assert customer.is_expired is False
        assert (
            Customer.objects.decode_session_key(customer.user.username)
            == request.session.session_key
        )
        customer.delete()
        try:
            Customer.objects.get(pk=customer.pk)
            self.fail("failed Customer found!")
        except Customer.DoesNotExist:
            pass

        try:
            get_user_model().objects.get(pk=customer.pk)
            self.fail("failed get_user_model() found!")
        except get_user_model().DoesNotExist:
            pass

    def test_authenticated_purchasing_user(self):
        """
        Check that an authenticated Django user creates a recognized django-SHOP customer.
        """
        user = get_user_model().objects.create_user(
            username="testuser", email="test@info.com", password="12345"
        )
        try:
            Customer.objects.get(pk=user.pk)
            self.fail("user found!")
        except Customer.DoesNotExist:
            pass

        request = self.factory.get("/shop")
        request.user = user
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        customer = Customer.objects.get_or_create_from_request(request)

        assert isinstance(customer, Customer)
        assert customer.is_anonymous is False
        assert customer.is_authenticated is True
        assert customer.is_recognized is True
        assert customer.is_guest is False
        assert customer.is_registered is True
        assert customer.is_visitor is False

    def test_authenticated_visiting_user(self):
        """
        Check that an authenticated user creates a recognized customer visiting the site.
        """
        user = get_user_model().objects.create_user(
            username="testuser", email="test@info.com", password="12345"
        )
        try:
            Customer.objects.get(pk=user.pk)
            self.fail("error")
        except Customer.DoesNotExist:
            pass

        request = self.factory.get("/shop")
        request.user = user
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()
        customer = Customer.objects.get_from_request(request)
        assert isinstance(customer, Customer)
        assert customer.is_authenticated is True
        assert customer.is_recognized is True
        assert customer.is_registered is True

    def test_authenticated_visiting_customer(self):
        """
        Check that an authenticated user creates a recognized customer visiting the site.
        """
        user = get_user_model().objects.create_user(
            username="testuser", email="test@info.com", password="12345"
        )
        customer = Customer.objects.create(user=user)

        assert isinstance(customer, Customer)
        assert isinstance(customer.user, get_user_model())
        assert customer.is_authenticated is True
        assert customer.is_registered is True

        request = self.factory.get("/shop")
        request.user = customer.user
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()
        customer = Customer.objects.get_from_request(request)

        assert isinstance(customer, Customer)
        assert customer.pk == request.user.pk
        assert customer.is_authenticated is True
        assert customer.is_recognized is True
        assert customer.is_registered is True
