from django.core.management.base import BaseCommand

# from digikala.spiders import digiCategory
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


class Command(BaseCommand):
    """
        Usage: python manage.py crawl category1 category2 category3
    """

    help = "release the spiders"

    def add_arguments(self, parser):
        parser.add_argument("categories", nargs="+", type=str)

    def handle(self, *args, **options):

        categories = " ".join(options["categories"])
        print(categories)

        process = CrawlerProcess(get_project_settings())

        process.crawl("digiCategory", categories=categories)
        process.start()
