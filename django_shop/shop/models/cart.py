from collections import OrderedDict
from django.db import models
from django.utils.translation import ugettext_lazy as _

from shop.models.fields import JSONField
from shop.models.customer import Customer

from django.utils.safestring import mark_safe

# from shop.modifiers.pool import cart_modifiers_pool
# from . import Money


class CartListItemManager(models.Manager):
    """
    Customized model manager for our CartListItem model.
    """

    def get_or_create(self, **kwargs):
        """
        Create a unique cart item. If the same product exists already in the given cart,
        increase its quantity, if the product in the cart seems to be the same.
        """
        cart = kwargs.pop("cart")
        list_item = kwargs.pop("list_item")
        quantity = int(kwargs.pop("quantity", 1))

        # add a new item to the cart, or reuse an existing one, increasing the quantity
        watched = not quantity  # if quantity is zero returns True
        cart_list_item = list_item.is_in_cart(cart)
        if cart_list_item:
            if not watched:
                cart_list_item.quantity += quantity
            created = False
        else:
            cart_list_item = self.model(
                cart=cart, list_item=list_item, quantity=quantity, **kwargs
            )
            created = True

        cart_list_item.save()
        return cart_list_item, created

    def filter_cart_items(self, cart, request):
        """
        Use this method to fetch items for shopping from the cart. It rearranges the result set
        according to the defined modifiers.
        """
        cart_items = self.filter(cart=cart, quantity__gt=0).order_by("updated_at")
        return cart_items


class CartListItem(models.Model):
    cart = models.ForeignKey(
        "Cart", related_name="cart_list_items", on_delete=models.CASCADE
    )
    list_item = models.ForeignKey("ListItem", on_delete=models.CASCADE)

    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    quantity = models.IntegerField(default=0)

    extra = JSONField(verbose_name=_("Arbitrary information for this cart list_item"))

    objects = CartListItemManager()

    class Meta:
        verbose_name = _("Cart list item")
        verbose_name_plural = _("Cart list items")

    def __init__(self, *args, **kwargs):
        # reduce the given fields to what the model actually can consume
        super(CartListItem, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        super(CartListItem, self).save(*args, **kwargs)
        self.cart.save(update_fields=["updated_at"])


class CartManager(models.Manager):
    """
    The Model Manager for any Cart inheriting from Cart.
    """

    def get_from_request(self, request):
        """
        Return the cart for current customer.
        """
        if request.customer.is_visitor:
            raise self.model.DoesNotExist("Cart for visiting customer does not exist.")
        if (
            not hasattr(request, "_cached_cart")
            or request._cached_cart.customer.user_id != request.customer.user_id
        ):
            request._cached_cart, created = self.get_or_create(
                customer=request.customer
            )
        return request._cached_cart

    def get_or_create_from_request(self, request):
        has_cached_cart = hasattr(request, "_cached_cart")
        if request.customer.is_visitor:
            request.customer = Customer.objects.get_or_create_from_request(request)
            has_cached_cart = False
        if (
            not has_cached_cart
            or request._cached_cart.customer.user_id != request.customer.user_id
        ):
            request._cached_cart, created = self.get_or_create(
                customer=request.customer
            )
        return request._cached_cart


class Cart(models.Model):
    """
    The fundamental part of a shopping cart.
    """

    customer = models.OneToOneField(
        "Customer",
        verbose_name=_("Customer"),
        related_name="cart",
        on_delete=models.CASCADE,
    )
    list_items = models.ManyToManyField(
        "ListItem", related_name="cart_set", through="CartListItem"
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    extra = JSONField(verbose_name=_("Arbitrary information for this cart"))

    # our CartManager determines the cart object from the request.
    objects = CartManager()

    class Meta:
        verbose_name = _("Shopping Cart")
        verbose_name_plural = _("Shopping Carts")

    def __init__(self, *args, **kwargs):
        super(Cart, self).__init__(*args, **kwargs)
        # That will hold things like tax totals or total discount
        self.extra_rows = OrderedDict()
        self._cached_cart_items = None
        self._dirty = True

    def save(self, force_update=False, *args, **kwargs):
        if self.pk or force_update is False:
            super(Cart, self).save(force_update=force_update, *args, **kwargs)
        self._dirty = True

    def update(self, request):
        """
        This should be called after a cart item changed quantity, has been added or removed.
        It will loop over all items in the cart, and call all the configured cart modifiers.
        After this is done, it will compute and update the order's total and subtotal fields, along
        with any supplement added along the way by modifiers.
        Note that theses added fields are not stored - we actually want to
        reflect rebate and tax changes on the *cart* items, but we don't want
        that for the order items (since they are legally binding after the
        "purchase" button was pressed)
        """
        if not self._dirty:
            return

        if self._cached_cart_items:
            items = self._cached_cart_items
        else:
            items = CartListItem.objects.filter_cart_items(self, request)

        self.extra_rows = OrderedDict()  # reset the dictionary
        self.subtotal = 0  # reset the subtotal
        for item in items:
            # item.update iterates over all cart modifiers and invokes method `process_cart_item`
            item.update(request)
            self.subtotal += item.line_total

        # Cache updated cart items
        self._cached_cart_items = items
        self._dirty = False

    def empty(self):
        """
        Remove the cart with all its items.
        """
        if self.pk:
            self.items.all().delete()
            self.delete()

    def merge_with(self, other_cart):
        """
        Merge the contents of the other cart into this one, afterwards delete it.
        This is done item by item, so that duplicate items increase the quantity.
        """
        # iterate over the cart and add quantities for items from other cart considered as equal
        if self.id == other_cart.id:
            raise RuntimeError("Can not merge cart with itself")
        for item in self.items.all():
            other_item = item.product.is_in_cart(other_cart, extra=item.extra)
            if other_item:
                item.quantity += other_item.quantity
                item.save()
                other_item.delete()

        # the remaining items from the other cart are merged into this one
        other_cart.items.update(cart=self)
        other_cart.delete()

    def __str__(self):
        return (
            f"Cart Pk:{self.pk} of customer: {self.customer.user.username}"
            if self.pk
            else "(unsaved)"
        )

    @property
    def num_items(self):
        """
        Returns the number of items in the cart.
        """
        return self.items.filter(quantity__gt=0).count()

    @property
    def total_quantity(self):
        """
        Returns the total quantity of all items in the cart.
        """
        aggr = self.items.aggregate(quantity=models.Sum("quantity"))
        return aggr["quantity"] or 0
        # if we would know, that self.items is already evaluated, then this might be faster:
        # return sum([ci.quantity for ci in self.items.all()])

    @property
    def is_empty(self):
        return self.num_items == 0 and self.total_quantity == 0

    @mark_safe
    def get_items(self):
        if self.list_items.all():
            return "</br>".join(
                [
                    f"List: <a href='{li.llist.get_absolute_url()}'>{li.llist}</a> \n Product:<a href='{li.product.get_absolute_url()}'>{li.product}</a>"
                    for li in self.list_items.all()
                ]
            )
        else:
            return "<empty cart>"
