from django.db import models
from django.urls import reverse

from functools import reduce
import operator
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from shop import models as shop_models

from shop.models.cart import CartListItem


class ProductManager(models.Manager):
    def select_lookup(self, search_term):
        """
        Returning a queryset containing the products matching the declared lookup fields together
        with the given search term. Each product can define its own lookup fields using the
        member list or tuple `lookup_fields`.
        """
        filter_by_term = (
            models.Q((sf, search_term)) for sf in self.model.lookup_fields
        )
        queryset = self.get_queryset().filter(reduce(operator.or_, filter_by_term))
        return queryset

    def indexable(self):
        """
        Return a queryset of indexable Products.
        """
        queryset = self.get_queryset().filter(active=True)
        return queryset

    def by_cart(self, cart):
        queryset = self.get_queryset().filter(lists__carts=cart)


class IntegerRangeField(models.IntegerField):
    def __init__(
        self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs
    ):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {"min_value": self.min_value, "max_value": self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("category-detail", args=[self.slug])


class Product(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    item_id = models.CharField(max_length=100)
    url = models.CharField(max_length=300)
    price = models.IntegerField()
    discount = IntegerRangeField(min_value=0, max_value=100)
    photo_url = models.CharField(max_length=255)
    available = models.BooleanField(default=True, blank=True)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)

    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    active = models.BooleanField(
        _("Active"), default=True, help_text=_("Is this product publicly visible.")
    )

    objects = ProductManager()

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def is_in_cart(self, cart):
        cart_item_qs = CartListItem.objects.filter(cart=cart, list_item__product=self)
        return cart_item_qs.first()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("product-detail", args=[self.id])
