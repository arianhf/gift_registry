from __future__ import unicode_literals

"""
Holds all the information relevant to the client (addresses for instance)
"""
from six import with_metaclass

from django.db import models
from django.template.loader import select_template
from django.utils.translation import ugettext_lazy as _

# from shop import deferred
# from shop.conf import app_settings


class AddressManager(models.Manager):
    def get_max_priority(self, customer):
        aggr = (
            self.get_queryset()
            .filter(customer=customer)
            .aggregate(models.Max("priority"))
        )
        priority = aggr["priority__max"] or 0
        return priority

    def get_fallback(self, customer):
        """
        Return a fallback address, whenever the customer has not declared one.
        """
        return self.get_queryset().filter(customer=customer).order_by("priority").last()


class BaseAddress(models.Model):

    priority = models.SmallIntegerField(
        default=0, db_index=True, help_text=_("Priority for using this address")
    )

    class Meta:
        abstract = True

    objects = AddressManager()

    def as_text(self):
        """
        Return the address as plain text to be used for printing, etc.
        """
        template_names = [
            # "{}/{}-address.txt".format(app_settings.APP_LABEL, self.address_type),
            # "{}/address.txt".format(app_settings.APP_LABEL),
            "shop/address.txt"
        ]
        template = select_template(template_names)
        return template.render({"address": self})


class ShippingAddress(BaseAddress):
    customer = models.ForeignKey("Customer", on_delete=models.CASCADE)
    address_type = "shipping"


class BillingAddress(BaseAddress):
    customer = models.ForeignKey("Customer", on_delete=models.CASCADE)
    address_type = "billing"

