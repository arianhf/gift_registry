from decimal import Decimal
import logging

from django.core.exceptions import ImproperlyConfigured
from django.db import models, transaction
from django.db.models.aggregates import Sum
from django.urls import NoReverseMatch, reverse
from django.utils.functional import cached_property
from django.utils.translation import (
    ugettext_lazy as _,
    pgettext_lazy,
    get_language_from_request,
)

from ipware.ip import get_ip
from shop.models.cart import CartListItem
from shop.models.fields import JSONField
from django_fsm import FSMField, transition
import random

# from shop.money. import Money
from shop.models.product import Product


class MockOrder:
    # MOCK FUNCTION
    def is_list_item_pruchased(self, item):
        if random.random() < 0.3:
            return True
        else:
            return False


class OrderQuerySet(models.QuerySet):
    def _filter_or_exclude(self, negate, *args, **kwargs):
        """
        Emulate filter queries on the Order model using a pseudo slug attribute.
        This allows to use order numbers as slugs, formatted by method `Order.get_number()`.
        """
        lookup_kwargs = {}
        for key, lookup in kwargs.items():
            try:
                index = key.index("__")
                field_name, lookup_type = key[:index], key[index:]
            except ValueError:
                field_name, lookup_type = key, ""
            if field_name == "slug":
                key, lookup = self.model.resolve_number(lookup).popitem()
                lookup_kwargs.update({key + lookup_type: lookup})
            else:
                lookup_kwargs.update({key: lookup})
        return super(OrderQuerySet, self)._filter_or_exclude(
            negate, *args, **lookup_kwargs
        )


class OrderManager(models.Manager):
    _queryset_class = OrderQuerySet

    def create_from_cart(self, cart, request):
        """
        This creates a new empty Order object with a valid order number (many payment service
        providers require an order number, before the purchase is actually completed). Therefore
        the order is not populated with any cart items yet; this must be performed in the next step
        by calling ``order.populate_from_cart(cart, request)``, otherwise the order object remains
        in state ``new``. The latter can happen, if a payment service provider did not acknowledge
        a payment, hence the items remain in the cart.
        """
        cart.update(request)
        cart.customer.get_or_assign_number()
        order = self.model(
            customer=cart.customer,
            currency=cart.total.currency,
            _subtotal=Decimal(0),
            _total=Decimal(0),
            stored_request=self.stored_request(request),
        )
        order.get_or_assign_number()
        order.assign_secret()
        order.save()
        return order

    def stored_request(self, request):
        """
        Extract useful information about the request to be used for emulating a Django request
        during offline rendering.
        """
        return {
            "language": get_language_from_request(request),
            "absolute_base_uri": request.build_absolute_uri("/"),
            "remote_ip": get_ip(request),
            "user_agent": request.META.get("HTTP_USER_AGENT"),
        }

    def get_summary_url(self):
        """
        Returns the URL of the page with the list view for all orders related to the current customer
        """
        pass


class Order(models.Model):
    """
    An Order is the "in process" counterpart of the shopping cart, which freezes the state of the
    cart on the moment of purchase. It also holds stuff like the shipping and billing addresses,
    and keeps all the additional entities, as determined by the cart modifiers.
    """

    TRANSITION_TARGETS = {
        "new": _("New order without content"),
        "created": _("Order freshly created"),
        "payment_confirmed": _("Payment confirmed"),
        "payment_declined": _("Payment declined"),
    }
    decimalfield_kwargs = {"max_digits": 30, "decimal_places": 2}
    decimal_exp = Decimal("." + "0" * decimalfield_kwargs["decimal_places"])

    customer = models.ForeignKey(
        "Customer",
        verbose_name=_("Customer"),
        related_name="orders",
        on_delete=models.PROTECT,
    )

    status = FSMField(default="new", protected=True, verbose_name=_("Status"))

    currency = models.CharField(
        max_length=7,
        editable=False,
        help_text=_("Currency in which this order was concluded"),
    )

    _subtotal = models.DecimalField(_("Subtotal"), **decimalfield_kwargs)

    _total = models.DecimalField(_("Total"), **decimalfield_kwargs)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)

    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    extra = JSONField(
        verbose_name=_("Extra fields"),
        help_text=_(
            "Arbitrary information for this order object on the moment of purchase."
        ),
    )

    stored_request = JSONField(
        help_text=_("Parts of the Request objects on the moment of purchase.")
    )

    objects = OrderManager()

    def __init__(self, *args, **kwargs):
        super(Order, self).__init__(*args, **kwargs)
        self.logger = logging.getLogger("shop.order")

    def __str__(self):
        return self.get_number()

    def __repr__(self):
        return "<{}(pk={})>".format(self.__class__.__name__, self.pk)

    def get_number(self):
        """
        Hook to get the order number.
        A class inheriting from Order may transform this into a string which is better readable.
        """
        return str(self.pk)

    @classmethod
    def round_amount(cls, amount):
        if amount.is_finite():
            return Decimal(amount).quantize(cls.decimal_exp)


class OrderPayment(models.Model):
    """
    A model to hold received payments for a given order.
    """

    order = models.ForeignKey(Order, verbose_name=_("Order"), on_delete=models.PROTECT)

    amount = models.DecimalField(
        decimal_places=2,
        verbose_name=_("Amount paid"),
        help_text=_("How much was paid with this particular transfer."),
        max_digits=15,
    )
    # )
    # MoneyField(
    #     _("Amount paid"),
    #     help_text=_("How much was paid with this particular transfer."),
    # )

    transaction_id = models.CharField(
        _("Transaction ID"),
        max_length=255,
        help_text=_("The transaction processor's reference"),
    )

    created_at = models.DateTimeField(_("Received at"), auto_now_add=True)

    payment_method = models.CharField(
        _("Payment method"),
        max_length=50,
        help_text=_("The payment backend used to process the purchase"),
    )

    class Meta:
        verbose_name = pgettext_lazy("order_models", "Order payment")
        verbose_name_plural = pgettext_lazy("order_models", "Order payments")

    def __str__(self):
        return _("Payment ID: {}").format(self.id)


class OrderItem(models.Model):
    """
    An item for an order.
    """

    order = models.ForeignKey(
        Order, related_name="items", verbose_name=_("Order"), on_delete=models.CASCADE
    )

    product_name = models.CharField(
        _("Product name"),
        max_length=255,
        null=True,
        blank=True,
        help_text=_("Product name at the moment of purchase."),
    )

    product_code = models.CharField(
        _("Product code"),
        max_length=255,
        null=True,
        blank=True,
        help_text=_("Product code at the moment of purchase."),
    )

    product = models.ForeignKey(
        Product,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Product"),
    )

    _unit_price = models.DecimalField(
        _("Unit price"),
        null=True,  # may be NaN
        help_text=_("Products unit price at the moment of purchase."),
        **Order.decimalfield_kwargs
    )

    _line_total = models.DecimalField(
        _("Line Total"),
        null=True,  # may be NaN
        help_text=_("Line total on the invoice at the moment of purchase."),
        **Order.decimalfield_kwargs
    )

    extra = JSONField(
        verbose_name=_("Extra fields"),
        help_text=_("Arbitrary information for this order item"),
    )

    class Meta:
        verbose_name = pgettext_lazy("order_models", "Ordered Item")
        verbose_name_plural = pgettext_lazy("order_models", "Ordered Items")

    def __str__(self):
        return self.product_name

    @property
    def unit_price(self):
        return self._unit_price
        # MoneyMaker(self.order.currency)(self._unit_price)

    @property
    def line_total(self):
        return self._line_total
        # MoneyMaker(self.order.currency)(self._line_total)

    # def populate_from_cart_item(self, cart_item, request):
    #     """
    #     From a given cart item, populate the current order item.
    #     If the operation was successful, the given item shall be removed from the cart.
    #     If a CartItem.DoesNotExist exception is raised, discard the order item.
    #     """
    #     if cart_item.quantity == 0:
    #         raise CartItem.DoesNotExist("Cart Item is on the Wish List")
    #     self.product = cart_item.product
    #     # for historical integrity, store the product's name and price at the moment of purchase
    #     self.product_name = cart_item.product.product_name
    #     self.product_code = cart_item.product_code
    #     self._unit_price = Decimal(cart_item.unit_price)
    #     self._line_total = Decimal(cart_item.line_total)
    #     self.quantity = cart_item.quantity
    #     self.extra = dict(cart_item.extra)
    #     extra_rows = [(modifier, extra_row.data) for modifier, extra_row in cart_item.extra_rows.items()]
    #     self.extra.update(rows=extra_rows)

    def save(self, *args, **kwargs):
        """
        Before saving the OrderItem object to the database, round the amounts to the given decimal places
        """
        self._unit_price = Order.round_amount(self._unit_price)
        self._line_total = Order.round_amount(self._line_total)
        super(OrderItem, self).save(*args, **kwargs)
