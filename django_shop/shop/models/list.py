from django.db import models
from shop.models.product import Product
from shop.models.cart import CartListItem
from django.urls import reverse
from django.contrib.auth.models import User
from django.conf import settings
from shop.models.fields import JSONField
from collections import OrderedDict
from django.utils.translation import ugettext_lazy as _


class List(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=300)
    products = models.ManyToManyField(Product, related_name="lists", through="ListItem")
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="lists"
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)

    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("list-detail", kwargs={"pk": self.id})


class ListItem(models.Model):
    llist = models.ForeignKey("List", related_name="items", on_delete=models.CASCADE)

    product = models.ForeignKey(
        Product, related_name="list_items", on_delete=models.CASCADE
    )

    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    extra = JSONField(verbose_name=_("Arbitrary information for this list item"))

    class Meta:
        verbose_name = _("List item")
        verbose_name_plural = _("List items")

    def __init__(self, *args, **kwargs):
        # reduce the given fields to what the model actually can consume
        super(ListItem, self).__init__(*args, **kwargs)
        self.extra_rows = OrderedDict()
        self._dirty = True

    def save(self, *args, **kwargs):
        super(ListItem, self).save(*args, **kwargs)
        self.llist.save(update_fields=["updated_at"])
        self._dirty = True

    def update(self, request):
        """
        Loop over all registered cart modifier, change the price per cart item and optionally add
        some extra rows.
        """
        if not self._dirty:
            return
        self.extra_rows = OrderedDict()  # reset the dictionary
        self._dirty = False

    def is_in_cart(self, cart):
        cart_item_qs = CartListItem.objects.filter(cart=cart, list_item=self)
        return cart_item_qs.first()
