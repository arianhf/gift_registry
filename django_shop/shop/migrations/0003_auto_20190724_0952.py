# Generated by Django 2.2.3 on 2019-07-24 09:52

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import shop.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20190724_0917'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='Created at'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='list',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated at'),
        ),
        migrations.CreateModel(
            name='ListItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('extra', shop.models.fields.JSONField(verbose_name='Arbitrary information for this list item')),
                ('llist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='shop.List')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop.Product')),
            ],
            options={
                'verbose_name': 'List item',
                'verbose_name_plural': 'List items',
            },
        ),
    ]
