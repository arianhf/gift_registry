from shop.models.customer import Customer


def customer(request):
    """
    Add the customer to the RequestContext
    """
    msg = "The request object does not contain a customer. Edit your MIDDLEWARE_CLASSES setting to insert 'shop.middlerware.CustomerMiddleware'."
    assert hasattr(request, "customer"), msg

    context = {"customer": request.customer}
    if request.user.is_staff:
        try:
            context.update(
                customer=Customer.objects.get(pk=request.session["emulate_user_id"])
            )
        except (Customer.DoesNotExist, KeyError, AttributeError):
            pass
    return context
