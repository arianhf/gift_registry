from django.utils.cache import add_never_cache_headers
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from shop.models.cart import Cart, CartListItem
from shop.models.product import Product
from shop.models.list import ListItem, List
from shop.models.customer import Customer
from shop.serializers.cart import CartSerializer, CartListItemSerializer
from django.shortcuts import render
from shop.serializers.product import ProductSerializer
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.http import HttpResponse
from django.contrib import messages


def cart_detail(request):
    cart = Cart.objects.get_or_create_from_request(request)
    return render(request, "shop/cart_detail.html", context={"cart": cart})


def remove_item_from_cart(request):
    id_ = request.GET.get("id")
    cart = Cart.objects.get_or_create_from_request(request)

    CartListItem.objects.get(id=id_).delete()
    messages.success(request, f"Item removed from your cart")

    django_messages = []

    for message in messages.get_messages(request):
        django_messages.append(
            {"level": message.level, "message": message.message, "tags": message.tags}
        )

    data = {}
    data["success"] = "ok"
    data["messages"] = django_messages
    return JsonResponse(data)


def add_item_to_cart(request):
    id_ = request.GET.get("id")
    pk = int(request.GET.get("pk"))
    print(type(pk))
    p = Product.objects.get(id=id_)
    print(pk)
    print(List.objects.get(id=pk))

    list_item = ListItem.objects.get(product=p, llist=List.objects.get(id=pk))
    cart = Cart.objects.get_or_create_from_request(request)

    CartListItem.objects.get_or_create(cart=cart, list_item=list_item, quantity=1)
    # items are not added to CART! #TODO:
    messages.success(request, f"Item added to your cart")

    django_messages = []

    for message in messages.get_messages(request):
        django_messages.append(
            {"level": message.level, "message": message.message, "tags": message.tags}
        )

    data = {}
    data["success"] = "ok"
    data["messages"] = django_messages
    return JsonResponse(data)


def update_cart_items(request):
    # cart = Cart.objects.get_from_request(request)
    # product_list = Products.objects.by_cart(cart=cart)
    customer = Customer.objects.get_from_request(request)
    return render(request, "shop/load_cart.html", {"customer": customer})


def reload_cart_items(request):
    cart = Cart.objects.get_or_create_from_request(request)
    return render(request, "shop/cart_items_detail.html", {"cart": cart})


def proceed_to_buy(request):
    # check for login type!
    if request.customer.is_authenticated:
        return render(request, "shop/start_buying_summary_page.html")
    else:
        if request.customer.is_guest:
            return HttpResponse(
                "<html><body>YOU ARE GUEST!<br>NOT AUTHENTICATED! AUTHENTICATE YOURSELF!</body></html>"
            )
        elif request.customer.is_recognized:
            return HttpResponse(
                "<html><body>YOU ARE RECOGNIZED!<br>NOT AUTHENTICATED! AUTHENTICATE YOURSELF!</body></html>"
            )
        else:
            return HttpResponse(
                f"<html><body>YOU ARE NOT RECOGNIZED!<br>{request.customer.recognized}NOT AUTHENTICATED! AUTHENTICATE YOURSELF!</body></html>"
            )


# class CartViewSet(viewsets.ModelViewSet):
#     serializer_label = "cart"
#     serializer_class = CartSerializer
#     item_serializer_class = CartItemSerializer

#     pagination_class = None

#     def get_queryset(self):
#         try:
#             cart = Cart.objects.get_from_request(self.request)
#             if self.kwargs.get(self.lookup_field):
#                 # we're interest only into a certain cart item
#                 return CartItem.objects.filter(cart=cart)
#             return cart
#         except Cart.DoesNotExist:
#             return Cart()

#     def list(self, request, *args, **kwargs):
#         cart = self.get_queryset()
#         context = self.get_serializer_context()
#         serializer = self.serializer_class(
#             cart, context=context, label=self.serializer_label
#         )
#         return Response(serializer.data)

#     def create(self, request, *args, **kwargs):
#         """
#         Create a new item in the cart.
#         """
#         context = self.get_serializer_context()
#         item_serializer = self.item_serializer_class(
#             context=context, data=request.data, label=self.serializer_label
#         )
#         item_serializer.is_valid(raise_exception=True)
#         self.perform_create(item_serializer)
#         headers = self.get_success_headers(item_serializer.data)
#         return Response(
#             item_serializer.data, status=status.HTTP_201_CREATED, headers=headers
#         )

#     def update(self, request, *args, **kwargs):
#         """
#         Handle changing the amount of the cart item referred by its primary key.
#         """
#         cart_item = self.get_object()
#         context = self.get_serializer_context()
#         item_serializer = self.item_serializer_class(
#             cart_item, context=context, data=request.data, label=self.serializer_label
#         )
#         item_serializer.is_valid(raise_exception=True)
#         self.perform_update(item_serializer)
#         cart_serializer = CartSerializer(cart_item.cart, context=context, label="cart")
#         response_data = {
#             "cart_item": item_serializer.data,
#             "cart": cart_serializer.data,
#         }
#         return Response(data=response_data)

#     def destroy(self, request, *args, **kwargs):
#         """
#         Delete a cart item referred by its primary key.
#         """
#         cart_item = self.get_object()
#         context = self.get_serializer_context()
#         cart_serializer = CartSerializer(
#             cart_item.cart, context=context, label=self.serializer_label
#         )
#         self.perform_destroy(cart_item)
#         response_data = {"cart_item": None, "cart": cart_serializer.data}
#         return Response(data=response_data)

#     @action(detail=True, methods=["get"])
#     def fetch(self, request):
#         cart = self.get_queryset()
#         context = self.get_serializer_context()
#         serializer = self.serializer_class(cart, context=context)
#         return Response(serializer.data)

#     @action(detail=False, methods=["get"], url_path="fetch-dropdown")
#     def fetch_dropdown(self, request):
#         cart = self.get_queryset()
#         context = self.get_serializer_context()
#         serializer = self.serializer_class(cart, context=context, label="dropdown")
#         return Response(serializer.data)
