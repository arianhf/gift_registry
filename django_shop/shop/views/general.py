from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib import messages
from shop.models import List, Product, Category
from django.views import generic
from shop.forms import ListForm
from django.http import JsonResponse
from users.models import User
from shop.models import Cart
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
def index(request):
    latest_products_list = Product.objects.order_by("-id")[:8]

    context = {"latest_products_list": latest_products_list}
    # Render the HTML template index.html with the data in the context variable
    return render(request, "shop/index.html", context=context)


class ProductDetailView(generic.DetailView):
    model = Product


class CategoryListView(generic.ListView):
    model = Category


class CategoryDetailView(generic.DetailView):
    model = Category
    slug_url_kwarg = "slug"


class ListDetailView(generic.DetailView):
    model = List

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            cart = Cart.objects.get_from_request(request)
            items = request.POST.getlist("addtocart")
            print(items)


class ListListView(generic.ListView):
    model = List


# class ListCreateView(generic.CreateView):
#     model = List
#     # fields = ("name", "description", "products")
#     form_class = ListForm
#     # TODO: change the way that products are shown in the page
#     # TODO: adding an add button that adds a search bar which searchs for items upon typing
#     # TODO: add a way to add items based on a link


class ListCreateView(LoginRequiredMixin, generic.FormView):
    template_name = "shop/list_form.html"
    fields = ("name", "description", "products")
    form_class = ListForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.save()
        messages.success(self.request, "List Created!")
        return super(ListCreateView, self).form_valid(form)

    # TODO: change the way that products are shown in the page
    # TODO: adding an add button that adds a search bar which searchs for items upon typing
    # TODO: add a way to add items based on a link


from rest_framework.decorators import api_view
from rest_framework.response import Response
from shop.serializers.product import ProductSerializer
from rest_framework import status

# class ProductViewSet(ModelViewSet):
#     queryset = Product.objects.all()
#     serializer_class = ProductSerializer


@api_view(["GET"])
def get_product(request, pk):
    p = Product.objects.get(id=pk)
    serializer = ProductSerializer(p)
    return Response(serializer.data, status=status.HTTP_200_OK)


def add_item_to_list(request):
    list_id = request.GET.get("id", None)
    list_id = int(list_id.split("_")[1])
    prod_id = request.GET.get("prod_id", None)
    prod_id = int(prod_id.split("_")[1])

    l = List.objects.get(id=list_id)
    p = Product.objects.get(id=prod_id)

    l.products.add(p)
    messages.success(request, f"Item added to {l.name}")

    django_messages = []

    for message in messages.get_messages(request):
        django_messages.append(
            {"level": message.level, "message": message.message, "tags": message.tags}
        )

    data = {}
    data["success"] = "ok"
    data["messages"] = django_messages
    return JsonResponse(data)


def show_user_lists(request, username):
    user = User.objects.get(username=username)
    lists = user.lists.all()

    context = {"user_shown": user, "lists": lists}
    return render(request, "shop/user_lists.html", context=context)

