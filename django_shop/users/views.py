from django.shortcuts import render, redirect
from .forms import ProfileUpdateForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.translation import gettext_lazy as _

from .models import User

from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from django.urls import reverse_lazy
from django.views import generic

from django.contrib.auth import logout, get_user_model
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import NON_FIELD_ERRORS
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from rest_auth.views import (
    LoginView as OriginalLoginView,
    PasswordChangeView as OriginalPasswordChangeView,
)

from shop.models.cart import Cart
from shop.models.customer import Customer
from shop.serializers.customer import CustomerSerializer

# from shop.serializers.auth import (
#     PasswordResetRequestSerializer,
#     PasswordResetConfirmSerializer,
# )

from django.http import HttpResponseRedirect


def anonymous_required(view_function, redirect_to=None):
    return AnonymousRequired(view_function, redirect_to)


class AnonymousRequired(object):
    def __init__(self, view_function, redirect_to):
        if redirect_to is None:
            from django.conf import settings

            redirect_to = settings.LOGIN_REDIRECT_URL
        self.view_function = view_function
        self.redirect_to = redirect_to

    def __call__(self, request, *args, **kwargs):
        if request.user is not None and request.user.is_authenticated:
            return HttpResponseRedirect(self.redirect_to)
        return self.view_function(request, *args, **kwargs)


class Register(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/register.html"


@login_required
def profile(request):
    if request.method == "POST":
        u_form = ProfileUpdateForm(request.POST, instance=request.user)
        if u_form.is_valid():
            u_form.save()
            messages.success(request, _("Your account has been updated!"))
            return redirect("profile")
    else:
        u_form = UserChangeForm(instance=request.user)

    context = {"u_form": u_form}
    return render(request, "registration/profile.html", context)


class AuthFormsView(APIView):
    """
    Generic view to handle authentication related forms such as user registration
    """

    form_class = None

    def post(self, request, *args, **kwargs):
        if request.customer.is_visitor:
            customer = Customer.objects.get_or_create_from_request(request)
        else:
            customer = request.customer
        form_data = requesshow_user_listst.data.get(self.form_class.scope_prefix, {})
        form = self.form_class(data=form_data, instance=customer)
        if form.is_valid():
            form.save(request=request)
            response_data = {
                form.form_name: {
                    "success_message": _("Successfully registered yourself.")
                }
            }
            return Response(response_data, status=status.HTTP_200_OK)
        errors = dict(form.errors)
        if "email" in errors:
            errors.update({NON_FIELD_ERRORS: errors.pop("email")})
        return Response(
            {form.form_name: errors}, status=status.HTTP_422_UNPROCESSABLE_ENTITY
        )


class LoginView(OriginalLoginView):
    form_name = "login_form"

    def login(self):
        """
        Logs in as the given user, and moves the items from the current to the new cart.
        """
        try:
            anonymous_cart = Cart.objects.get_from_request(self.request)
        except Cart.DoesNotExist:
            anonymous_cart = None
        if (
            self.request.customer.user.is_anonymous
            or self.request.customer.is_registered
        ):
            previous_user = None
        else:
            previous_user = self.request.customer.user
        super(LoginView, self).login()  # this rotates the session_key
        if not self.serializer.data.get("stay_logged_in"):
            self.request.session.set_expiry(0)  # log out when the browser is closed
        authenticated_cart = Cart.objects.get_from_request(self.request)
        if anonymous_cart:
            # an anonymous customer logged in, now merge his current cart with a cart,
            # which previously might have been created under his account.
            authenticated_cart.merge_with(anonymous_cart)
        if (
            previous_user
            and previous_user.is_active is False
            and previous_user != self.request.user
        ):
            # keep the database clean and remove this anonymous entity
            if previous_user.customer.orders.count() == 0:
                previous_user.delete()

    def post(self, request, *args, **kwargs):
        self.request = request
        form_data = request.data.get("form_data", {})
        print(form_data)
        self.serializer = self.get_serializer(data=form_data)
        if self.serializer.is_valid():
            self.login()
            return self.get_response()

        exc = ValidationError({self.form_name: self.serializer.errors})
        response = self.handle_exception(exc)
        self.response = self.finalize_response(request, response, *args, **kwargs)
        return self.response


class LogoutView(APIView):
    """
    Calls Django logout method and delete the auth Token assigned to the current User object.
    """

    permission_classes = (AllowAny,)
    form_name = "logout_form"

    def post(self, request):
        try:
            request.user.auth_token.delete()
        except:
            pass
        logout(request)
        request.user = AnonymousUser()
        response_data = {
            self.form_name: {"success_message": _("Successfully logged out.")}
        }
        return Response(response_data)


# class ContinueAsGuest(APIView):
#     serializer_class = Customer
#     form_class = None

#     def post(self, request, *args, **kwargs):
#         if request.customer.is_visitor:
#             customer = Customer.objects.get_or_create_from_request(request)
#         else:
#             customer = request.customer
#         form_data = request.data.get(self.form_class.scope_prefix, {})
#         form = self.form_class(data=form_data, instance=customer)
#         if form.is_valid():
#             form.save(request=request)
#             response_data = {
#                 form.form_name: {
#                     "success_message": _("Successfully registered yourself.")
#                 }
#             }
#             return Response(response_data, status=status.HTTP_200_OK)
#         errors = dict(form.errors)
#         if "email" in errors:
#             errors.update({NON_FIELD_ERRORS: errors.pop("email")})
#         return Response(
#             {form.form_name: errors}, status=status.HTTP_422_UNPROCESSABLE_ENTITY
#         )


# class Register(APIView):
#     serializer_class = Customer
#     form_class = None

#     def post(self, request, *args, **kwargs):
#         if request.customer.is_visitor:
#             customer = Customer.objects.get_or_create_from_request(request)
#         else:
#             customer = request.customer
#         form_data = request.data.get(self.form_class.scope_prefix, {})
#         form = self.form_class(data=form_data, instance=customer)
#         if form.is_valid():
#             form.save(request=request)
#             response_data = {
#                 form.form_name: {
#                     "success_message": _("Successfully registered yourself.")
#                 }
#             }
#             return Response(response_data, status=status.HTTP_200_OK)
#         errors = dict(form.errors)
#         if "email" in errors:
#             errors.update({NON_FIELD_ERRORS: errors.pop("email")})
#         return Response(
#             {form.form_name: errors}, status=status.HTTP_422_UNPROCESSABLE_ENTITY
#         )

