from django.test import TestCase
import pytz
import re
from datetime import timedelta
from django.conf import settings
from django.urls import reverse
from django.utils.timezone import datetime
from rest_framework.test import APIClient, APIRequestFactory
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from shop.models import Customer


class AuthTest(TestCase):
    def setUp(self):
        self.api_client = APIClient()
        self.api_factory = APIRequestFactory()
        # Every test needs access to the request factory.
        model = get_user_model()

        user = model.objects.create(
            email="admin@example.com", password=make_password("secret"), is_active=True
        )
        assert isinstance(user, get_user_model())
        assert user.is_authenticated is True
        self.user = user

        customer = Customer.objects.create(user=user)
        assert isinstance(customer, Customer)
        assert isinstance(customer.user, get_user_model())
        assert customer.is_authenticated is True
        assert customer.is_registered is True

        self.registered_customer = customer

    # def test_visiting_customer(self):
    #     request = self.factory.get("/shop")
    #     request.user = AnonymousUser()
    #     middleware = SessionMiddleware()
    #     middleware.process_request(request)
    #     request.session.save()

    def test_login_fail(self):
        login_url = reverse("login")
        data = {"form_data": {"username": "a", "password": "b"}}
        response = self.api_client.post(login_url, data, format="json")
        assert response.status_code == 400
        assert response.json() == {
            "login_form": {
                "non_field_errors": ["Unable to log in with provided credentials."]
            }
        }
        assert response.cookies.get("sessionid") is None

    def test_login_success(self):
        login_url = reverse("login")
        data = {
            "form_data": {
                "username": self.registered_customer.email,
                "password": "secret",
            }
        }
        response = self.api_client.post(login_url, data, format="json")

        assert response.status_code == 200
        assert len(response.json().get("key")) == 40
        session_cookie = response.cookies.get("sessionid")
        assert session_cookie["expires"] == ""
        assert session_cookie["max-age"] == ""

    def test_login_presistent(self):
        login_url = reverse("login")
        data = {
            "form_data": {
                "username": self.registered_customer.email,
                "password": "secret",
                "stay_logged_in": True,
            }
        }
        response = self.api_client.post(login_url, data, format="json")
        request = self.api_factory.post(login_url, data, format="json")

        tz_gmt = pytz.timezone("GMT")
        shall_expire = datetime.now(tz=tz_gmt).replace(microsecond=0) + timedelta(
            seconds=settings.SESSION_COOKIE_AGE
        )
        assert response.status_code == 200
        session_cookie = response.cookies.get("sessionid")

        # print("\n\n")

        # print(session_cookie)
        # for k, v in session_cookie.items():
        #     print(k, v)

        # print("\n\n")
        expires = datetime.strptime(
            session_cookie["expires"], "%a, %d-%b-%Y %H:%M:%S GMT"
        ).replace(tzinfo=tz_gmt)
        assert abs(expires - shall_expire) < timedelta(seconds=5)
        assert session_cookie["max-age"] == settings.SESSION_COOKIE_AGE

    def test_logout(self):
        assert (
            self.api_client.login(
                username=self.registered_customer.email, password="secret"
            )
            is True
        )
        logout_url = reverse("logout")
        response = self.api_client.post(logout_url, {}, format="json")
        assert response.status_code == 200
        assert response.json() == {
            "logout_form": {"success_message": "Successfully logged out."}
        }

    def test_change_password_fail(self):
        pass  # TODO

    def test_change_password_success(self):
        pass  # TODO

    def test_password_reset(self):
        pass  # TODO

    def test_password_reset_fail(self):
        pass  # TODO

    def test_register_user_with_password(self):
        """
        Test if a new user can register himself providing his own new password.
        """
        register_user_url = reverse("register")
        data = {
            "form_data": {
                "email": "newby@example.com",
                "password1": "secret",
                "password2": "secret",
                "preset_password": False,
            }
        }
        response = self.api_client.post(register_user_url, data, format="json")
        print(response.status_code)
        assert response.status_code == 200
        assert response.json() == {
            "register_user_form": {
                "success_message": "Successfully registered yourself."
            }
        }
        customer = Customer.objects.get(user__email="newby@example.com")
        assert customer is not None

    def test_register_user_generate_password(self):
        pass  # TODO

    def test_register_user_fail(self):
        """
        Test if a new user cannot register himself, if that user already exists.
        """
        register_user_url = reverse("register")
        data = {
            "form_data": {
                "email": self.registered_customer.email,
                "password1": "",
                "password2": "",
                "preset_password": True,
            }
        }
        response = self.api_client.post(register_user_url, data, format="json")
        assert response.status_code == 422
        assert response.json() == {
            "register_user_form": {
                "__all__": [
                    "A customer with the e-mail address 'admin@example.com' already exists.\nIf you have used this address previously, try to reset the password."
                ]
            }
        }

    def test_registered_user_cant_continue_as_guest(self):
        pass

    def test_registered_user_cant_register(self):
        pass

