from django.contrib.auth import views as auth_views
from django.urls import path, re_path, include
from . import views

from users.forms import RegisterUserForm, ContinueAsGuestForm
from users.views import (
    AuthFormsView,
    LoginView,
    LogoutView,
    # PasswordChangeView,
    # PasswordResetRequestView,
)
from users.views import anonymous_required


urlpatterns = [
    # path(
    #     "password/reset/",
    #     PasswordResetRequestView.as_view(),
    #     name="password-reset-request",
    # ),
    # path("login/", LoginView.as_view(), name="login"),
    path(
        "register/",
        anonymous_required(AuthFormsView.as_view(form_class=RegisterUserForm)),
        name="register",
    ),
    path(
        "continue/",
        anonymous_required(AuthFormsView.as_view(form_class=ContinueAsGuestForm)),
        name="continue-as-guest",
    ),
    path("logout/", LogoutView.as_view(), name="logout"),
    #path("password/change/", PasswordChangeView.as_view(), name="password-change"),
    path(
        "login/",
        auth_views.LoginView.as_view(redirect_authenticated_user=True),
        name="login",
    ),
    #path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    # path("register/", views.Register.as_view(), name="register"),
    path("profile/", views.profile, name="profile"),
    path("", include("django.contrib.auth.urls")),
]
