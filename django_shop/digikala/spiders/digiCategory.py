# -*- coding: utf-8 -*-
import scrapy
from ..items import DigikalaCategoryItem, DigikalaProductItem
from unidecode import unidecode


class DigicategorySpider(scrapy.Spider):
    name = "digiCategory"
    allowed_domains = ["digikala.com"]
    start_urls = ["https://www.digikala.com/search/category-women-earrings/"]
    page_number = 1

    def __init__(self, *args, **kwargs):
        super(DigicategorySpider, self).__init__(*args, **kwargs)

        if "categories" in kwargs:
            categories = kwargs.get("categories").split()
            self.start_urls = [
                f"https://www.digikala.com/search/{category}/?sortby=4"
                for category in categories
            ]

    def parse(self, response):
        data = response.css(".is-plp")
        print(len(data))
        if len(data) == 0:
            return
        # print(data)
        for item in data:
            itemCat = DigikalaCategoryItem()
            itemProduct = DigikalaProductItem()

            item_id = (
                item.css(".c-product-box__title a")
                .css("::attr(href)")
                .extract()[0]
                .split("/")[2]
            )

            # get data about the Category

            slug = response.request.url.split("/")[-2]
            category_name = (
                response.css(".c-breadcrumb li:last-child span")
                .css("::text")
                .extract()[0]
            )
            itemCat["name"] = category_name
            itemCat["slug"] = slug

            # get data about the Link
            itemProduct["url"] = (
                item.css(".c-product-box__title a").css("::attr(href)").extract()[0]
            )
            # get data about the Price
            try:
                itemProduct["price"] = int(
                    unidecode(
                        item.css(".c-price__value-wrapper::text")
                        .extract()[0]
                        .strip()
                        .replace(",", "")
                    )
                )
            except:
                itemProduct["price"] = 0
            itemProduct["discount"] = 0  # Not needed yet

            # get data about the Photo
            # itemPhoto["img"] = None  # added in the pipeline
            itemProduct["photo_url"] = (
                item.css(".js-product-item img").css("::attr(src)").extract()[0]
            )
            # get data about the product name
            itemProduct["name"] = (
                item.css(".c-product-box__title a").css("::text").extract()[0]
            )
            itemProduct["category"] = slug
            itemProduct["item_id"] = item_id
            print(itemProduct["item_id"])

            yield itemCat
            yield itemProduct

        if response.css(".c-pager__next") is not None:
            pos_q = response.request.url.find("?")
            pos_pageno = response.request.url.find("?pageno=")
            if pos_pageno != -1:
                pos_and = response.request.url.find("&")
                self.page_number = int(response.request.url[pos_pageno + 8 : pos_and])
            next_page = response.request.url[:pos_q]
            next_page += f"?pageno={self.page_number+1}&sortby=4"
            print(next_page)

            yield response.follow(next_page, callback=self.parse)
