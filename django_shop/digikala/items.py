# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DigikalaItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


from scrapy_djangoitem import DjangoItem
from shop.models.product import Category, Product


class DigikalaCategoryItem(DjangoItem):
    django_model = Category


class DigikalaProductItem(DjangoItem):
    django_model = Product
