# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import urllib
import os
from django.core.files import File
from digikala.items import DigikalaCategoryItem, DigikalaProductItem

from shop.models.product import Category, Product


class DigikalaPipeline(object):
    def process_item(self, item, spider):
        if isinstance(item, DigikalaCategoryItem):
            obj, created = Category.objects.get_or_create(
                name=item["name"], slug=item["slug"]
            )
        elif isinstance(item, DigikalaProductItem):
            item["category"] = Category.objects.get(slug=item["category"])

            item.save()
        # elif isinstance(item, DigikalaLinkItem):
        #     item["url"] = "https://digikala.com" + item["url"]
        #     item["product"] = Product.objects.get(item_id=item["product"])
        #     item.save()
        # elif isinstance(item, DigikalaPhotoItem):
        #     link = item["url"]
        #     item["product"] = Product.objects.get(item_id=item["product"])
        #     item.save()
        # elif isinstance(item, DigikalaPriceItem):
        #     item["product"] = Product.objects.get(item_id=item["product"])
        #     item.save()

        return item
